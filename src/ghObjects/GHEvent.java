package ghObjects;

import com.jcabi.github.Event;

import java.io.IOException;
import java.util.Date;

public class GHEvent {
    private String number;
    private String type;
    private String user;
    private Date date;
    private String json;

    public GHEvent(Event.Smart event) throws IOException {
        number = event.number()+"";
        type = event.type();
        user = event.author().login();
        date = event.createdAt();
        json = event.json().toString();
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public Date getDate() {
        return date;
    }

    public String getUser() {
        return user;
    }

    public String getJson() {
        return json;
    }
}
