package ghObjects;

import com.jcabi.github.RepoCommit;

import javax.json.JsonObject;
import java.io.IOException;

public class GHContributor {
    private String name, email;
    private int totalSentiment=0, commitCount=0, maxSentiment = -5, minSentiment = 5;


    public GHContributor(RepoCommit.Smart commit) throws IOException {
        name = getCommitterNameFromCommit(commit.json());
        email = getCommitterEmailFromCommit(commit.json());
    }

    private String getCommitterNameFromCommit(JsonObject json) {
        return json.getJsonObject("commit").getJsonObject("author").getString("name");
    }

    private String getCommitterEmailFromCommit(JsonObject json) {
        return json.getJsonObject("commit").getJsonObject("author").getString("email");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTotalSentiment() {
        return totalSentiment;
    }

    public void setTotalSentiment(int totalSentiment) {
        this.totalSentiment = totalSentiment;
    }

    public int getCommitCount() {
        return commitCount;
    }

    public void setCommitCount(int commitCount) {
        this.commitCount = commitCount;
    }

    public double getAverageSentiment(){
        if(commitCount>0)
            return (double)totalSentiment/commitCount;
        return 0d;
    }

    public int getMaxSentiment() {
        return maxSentiment;
    }

    public void setMaxSentiment(int maxSentiment) {
        this.maxSentiment = maxSentiment;
    }

    public int getMinSentiment() {
        return minSentiment;
    }

    public void setMinSentiment(int minSentiment) {
        this.minSentiment = minSentiment;
    }

    @Override
    public String toString() {
        return "GHContributor{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public void addNewSentiment(int newSentiment) {
        totalSentiment += newSentiment;
        commitCount++;

        if(newSentiment > maxSentiment)
            maxSentiment = newSentiment;

        if(newSentiment < minSentiment)
            minSentiment = newSentiment;
    }
}
