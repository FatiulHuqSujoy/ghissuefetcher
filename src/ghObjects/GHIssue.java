package ghObjects;

import com.jcabi.github.Issue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class GHIssue {
    private String id, title, body, json;
    private Date createdDate, closedDate;
    private boolean isOpen;
    private List<GHComment> comments;
    private List<String> labels;
    private List<GHEvent> events;

    public GHIssue(){
        comments = new ArrayList<>();
        labels = new ArrayList<>();
        events = new ArrayList<>();
    }

    public GHIssue(Issue.Smart issue) {
        this();

        id = issue.number() + "";
        try {
            title = issue.title();
            body = issue.body();
            createdDate = issue.createdAt();

            json = issue.json().toString();
            System.out.println("issue json: " + json);

            if(issue.isOpen()){
                isOpen = true;
            }else{
                isOpen = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addComment(GHComment ghComment) {
        comments.add(ghComment);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<GHComment> getComments() {
        return comments;
    }

    public void setComments(List<GHComment> comments) {
        this.comments = comments;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    @Override
    public String toString() {
        return "GHPullRequest{" + "\'\n" +
                "id='" + id + "\'\n" +
                "title='" + title + "\'\n" +
                "body='" + body + "\'\n" +
                "createdDate=" + createdDate + "\n" +
                "mergedDate=" + closedDate + "\n" +
                "isOpen=" + isOpen + "\n\n" +
                "comments=\n" + comments + "\n" +
                '}' + "\n";
    }

    public String getJsonOutput(){
        return "{" +
                "\"id\":\"" + id + "\", " +
                "\"title\":\"" + title + "\", " +
                "\"body\":\"" + body + "\", " +
                "\"json\":\"" + json + "\", " +
                "\"commentIds\":[" + getContentIds() + "], " +
                "\"labels\":[" + getLabelNames() + "]" +
//                "\"eventNumbers\":[" + getEventNumbers() + "]" +
                "}";
    }

    private String getEventNumbers() {
        if(events.isEmpty())
            return "";

        String numbers = "";
        for(GHEvent ghEvent : events){
            numbers += "\"" + ghEvent.getNumber() + "\",";
        }

        return numbers.substring(0,numbers.length()-1);
    }

    private String getContentIds() {
        if(comments.isEmpty())
            return "";

        String ids = "";
        for(GHComment pullContent : comments){
            ids += "\"" + pullContent.getId() + "\",";
        }

        return ids.substring(0,ids.length()-1);
    }

    private String getLabelNames() {
        if(labels.isEmpty())
            return "";

        String labelNames = "";
        for(String label : labels){
            labelNames += "\"" + label + "\",";
        }

        return labelNames.substring(0,labelNames.length()-1);
    }

    public void sortContents() {
        sortThisContentList(comments);
    }

    private <G extends GHComment> void sortThisContentList(List<G> contents) {
        if(!contents.isEmpty())
            Collections.sort(contents);
    }

    public void addLabel(String labelStr) {
        labels.add(labelStr);
    }

    public void addEvent(GHEvent event) {
        events.add(event);
    }

    public List<GHEvent> getEvents() {
        return events;
    }
}
