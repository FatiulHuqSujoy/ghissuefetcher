package ghObjects;

import com.jcabi.github.Comment;

import java.io.IOException;
import java.util.Date;

public class GHComment implements Comparable<GHComment>{
    protected String id;
    protected String body;
    protected Date date;
    protected String json;

    public GHComment(Comment.Smart comment) throws IOException {
        id = comment.number()+"";
        body = comment.body();
        date = comment.createdAt();
        json = comment.json().toString();
    }

    @Override
    public String toString() {
        return "GHComment{" + "\n" +
                "id='" + id + "\'\n" +
                "body='" + body + "\'\n" +
                "date=" + date + "\n" +
                '}' + "\n";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(GHComment o) {
        return date.compareTo(o.getDate());
    }
}
