package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileProcessor {
    public static List<String> listStringsFromFile(File file) throws FileNotFoundException {
        List<String> listOfStrings = new ArrayList<>();
        if(file.exists()){
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()){
                listOfStrings.add(scanner.next());
            }
            scanner.close();
        }
        return listOfStrings;
    }

    public static List<String> listFileNamesInFolder(File folder, String fileExtension) {
        List<String> fileNameList = new ArrayList<>();

        for (final File fileEntry : folder.listFiles()) {
            String fileName = fileEntry.getName();
            String prNumber = fileName.replace(fileExtension, "");
            fileNameList.add(prNumber);
        }

        return fileNameList;
    }
}
