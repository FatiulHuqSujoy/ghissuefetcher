package utils;

public class JsonStringProcessor {

    public static String processJsonString(String prJson) {
        String processedJson;
//        processedJson = replaceFaultyJsonString(prJson);
        processedJson = replaceFaultyBodyString(prJson);
        processedJson = replaceFaultyTitleString(processedJson);
//        System.out.println(prJson + "\n" + processedJson + "\n");
        return processedJson;
    }

    private static String replaceFaultyJsonString(String prJson) {
        String innerJson = prJson
                .substring(prJson.indexOf("\"json\":\"{"), prJson.indexOf("}\", \"commitIds\""))
                .replace("\"json\":\"{", "")
                .replace("}\", \"commitIds\"", "");
        String correctInnerJson = innerJson
                .replace("\\\"", "\"")
                .replace("\"", "\\\"");

        return prJson.replace(innerJson, correctInnerJson);
    }

    private static String replaceFaultyBodyString(String prJson) {
        String innerJson = prJson
                .substring(prJson.indexOf("\"body\":\""), prJson.indexOf("\", \"json\""))
                .replace("\"body\":\"", "")
                .replace("\", \"json\"", "");
        String correctInnerJson = innerJson
                .replace("\\", "\\\\")
                .replace("\"", "\\\"")
//                .replace("\n", "\\n ")
//                .replace("\r", "\\r")
                ;

        return prJson.replace(innerJson, correctInnerJson);
    }

    private static String replaceFaultyTitleString(String prJson) {
        String innerJson = prJson
                .substring(prJson.indexOf("\"title\":\""), prJson.indexOf("\", \"body\""))
                .replace("\"title\":\"", "")
                .replace("\", \"body\"", "");
        String correctInnerJson = innerJson.replace("\"", "\\\"");

        return prJson.replace(innerJson, correctInnerJson);
    }
}
