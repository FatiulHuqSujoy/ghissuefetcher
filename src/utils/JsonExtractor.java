package utils;

import com.jcabi.github.Commit;
import com.jcabi.github.PullComment;

import javax.json.JsonObject;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JsonExtractor {
    public static Date getDateFromPullComment(JsonObject jsonObject) {
        Date date = new Date();
        try {
            String pullCommentDate = jsonObject.
                    getString("created_at").split("T")[0];
            date = new SimpleDateFormat("yyyy-MM-dd").parse(pullCommentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getMessageFromPullComment(JsonObject jsonObject) {
        String pullCommentMessage = jsonObject.getString("body");
        return pullCommentMessage;
    }

    public static String getIdFromPullComment(JsonObject jsonObject) {
        String pullCommentMessage = jsonObject.getInt("id")+"";
        return pullCommentMessage;
    }

    public static String getMessageFromCommit(JsonObject jsonObject) {
        String commitMessage = jsonObject.getString("message");
        return commitMessage;
    }

    public static Date getDateFromCommit(JsonObject jsonObject) {
        Date date = new Date();
        try {
            String commitDate = jsonObject.
                    getJsonObject("author").
                    getString("date").split("T")[0];
            date = new SimpleDateFormat("yyyy-MM-dd").parse(commitDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getTitleFromPullRequest(JsonObject json) {
        String title = json.getString("title");
        return title;
    }

    public static String getBodyFromPullRequest(JsonObject json) {
        if(json.isNull("body")) {
            return "";
        }
        String body = json.getString("body");
        return body;
    }
}
