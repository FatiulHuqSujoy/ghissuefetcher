package utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.List;

public class JSONConverter {
    public static String jsonResult(Object object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }
}
