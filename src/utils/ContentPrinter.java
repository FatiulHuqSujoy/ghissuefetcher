package utils;

import ghObjects.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ContentPrinter {
    private static final String RESULT_DIR = "results/";
    private String projectName;

    public ContentPrinter(String projectName) {
        this.projectName = projectName;
        createDirectory(RESULT_DIR + projectName);
    }

    private void printContent(String text, String id, String dirName){
        String filePath = RESULT_DIR + projectName + "/" + dirName;
        createDirectory(filePath);
        FindingsWriter.printResultInTextFile(text, filePath + "/" + id);
    }

    public void printIssue(GHIssue issue){
        for(GHComment comment : issue.getComments()){
            String text = JSONConverter.jsonResult(comment);
            printContent(text, comment.getId(), "comments");
        }

//        for(GHEvent event : issue.getEvents()){
//            String text = JSONConverter.jsonResult(event);
//            printContent(text, event.getNumber(), "events");
//        }

        String output = JsonStringProcessor.processJsonString(issue.getJsonOutput());
        printContent(output, issue.getId(), "issues");
    }

    public void printAllContents(List<GHIssue> IssueList){
        for(GHIssue Issue : IssueList){
            printIssue(Issue);
        }
    }

    public static void createDirectory(String path) {
        try {
            FileUtils.forceMkdir(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
