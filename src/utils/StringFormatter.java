package utils;

public class StringFormatter {
    public static String getDivisionAsString(double division, int decimal) {
        String format = "%." + decimal + "f";
        return String.format(format, division);
    }

    public static String getPercentageAsString(int x, int y) {
        double division = (double) x*100/y;
        String result = getDivisionAsString(division, 2) + "%";
        return result;
    }
}
