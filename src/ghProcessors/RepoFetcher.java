package ghProcessors;

import com.jcabi.github.Coordinates;
import com.jcabi.github.Github;
import com.jcabi.github.Repo;
import com.jcabi.github.RtGithub;

public class RepoFetcher {
    private static Github github = new RtGithub("2cd3f6502755165cea73264d4a235431d506c180");

    public static Repo fetchRepo(String url){

        String projectName = parseProjectNameFromUrl(url);
        Coordinates coords = new Coordinates.Simple(projectName);
        Repo repo = github.repos().get(coords);

        return repo;
    }

    private static String parseProjectNameFromUrl(String url) {
        return url;
    }
}
