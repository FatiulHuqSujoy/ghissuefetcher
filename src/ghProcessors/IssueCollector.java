package ghProcessors;

import com.jcabi.github.*;
import ghObjects.GHEvent;
import ghObjects.GHIssue;
import ghObjects.GHComment;
import utils.ContentPrinter;

import java.io.IOException;
import java.util.*;

public class IssueCollector {
    private final int MAX_COMMENT_COUNT = 100000;
    private final int MAX_ISSUE_COUNT = 100000;

    private List<GHIssue> issues;
    private List<String> processedIssueNumbers;
    private PullRequestLister pullRequestLister;

    private ContentPrinter contentPrinter;

    public IssueCollector(ContentPrinter contentPrinter, List<String> processedIssueNumbers, PullRequestLister pullRequestLister){
        issues = new ArrayList<>();
        this.contentPrinter = contentPrinter;
        this.processedIssueNumbers = processedIssueNumbers;
        this.pullRequestLister = pullRequestLister;
        pullRequestLister.populatePRIds();
    }

    public void fetchIssues(Repo repo) throws IOException{
        long startTime = System.currentTimeMillis();

        int count=1;
        for(Issue issue1 : repo.issues().iterate(getIssueParams())){
            Issue.Smart issue = new Issue.Smart(issue1);

            if(issueIneligible(issue))
                continue;

            GHIssue ghIssue = new GHIssue(issue);
            System.out.println(count + ": " + ghIssue.getId());

//            iterateEvents(issue1, ghIssue);
            iterateComments(issue, ghIssue);
            iterateLabels(issue, ghIssue);

//            ghIssue.sortContents();

            issues.add(ghIssue);

            contentPrinter.printIssue(ghIssue);

            if(count == MAX_ISSUE_COUNT)
                break;

            count++;
        }

        generateExecutionTime(startTime, System.currentTimeMillis());
    }

    private boolean issueIneligible(Issue.Smart issue) throws IOException {
        String id = issue.number() + "";
        if(pullRequestLister.isPR(id))
            return true;

        if(issueAlreadyProcessed(id))
            return true;

        if (issue.isPull()){
            pullRequestLister.addPRId(id);
            return true;
        }

        return false;
    }

    private boolean issueAlreadyProcessed(String id) {
        if(processedIssueNumbers.contains(id)) {
            System.out.println("already processed: " + id);
            return true;
        }

        return false;
    }

    private Map<String, String> getIssueParams() {
        Map<String, String> params = new HashMap<>();
        params.put("state", "closed");
//        params.put("sort", "popularity");
//        params.put("direction", "desc");
        return params;
    }

    private void iterateLabels(Issue.Smart issue, GHIssue ghIssue) {
        for(Label label: issue.labels().iterate()){
            String labelStr = label.name();
            ghIssue.addLabel(labelStr);
        }
    }

    private void iterateComments(Issue.Smart issue, GHIssue ghIssue) throws IOException {
        int count = 1;
        for(Comment comment1: issue.comments().iterate(new Date(0))){
            Comment.Smart comment = new Comment.Smart(comment1);

            GHComment ghComment = new GHComment(comment);
            ghIssue.addComment(ghComment);

            if(count == MAX_COMMENT_COUNT)
                break;

            count++;
        }
    }

    private void iterateEvents(Issue issue, GHIssue ghIssue) throws IOException {
        Iterator<Event> eventIterator = issue.events().iterator();
        while (eventIterator.hasNext()){
            System.out.println(eventIterator.next().number());
//            Event.Smart event = new Event.Smart(eventIterator.next());
//            GHEvent ghEvent = new GHEvent(event);
//            ghIssue.addEvent(ghEvent);
        }
    }

    private void generateExecutionTime(long startTime, long endTime) {
        long executionTime = (endTime-startTime)/(long)(1000);

        System.out.println("EXECUTION TIME: " + executionTime);
    }

    public List<GHIssue> getIssues() {
        return issues;
    }
}
