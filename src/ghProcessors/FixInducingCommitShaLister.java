package ghProcessors;

import utils.FileProcessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class FixInducingCommitShaLister {
    private String fileName;
    private final String FOLDER_NAME = "fix_inducing_commits/";

    public FixInducingCommitShaLister(String projectName) {
        fileName = projectName + "_commits.txt";
    }

    public List<String> listCommits() throws FileNotFoundException {
        File file = new File(FOLDER_NAME + fileName);
        List<String> list = FileProcessor.listStringsFromFile(file);

        return list;
    }
}
