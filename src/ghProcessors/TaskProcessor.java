package ghProcessors;

import com.jcabi.github.Repo;
import ghObjects.GHIssue;
import org.eclipse.jgit.api.errors.GitAPIException;
import utils.ContentPrinter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class TaskProcessor {
    private String projectPath, projectName;

    private List<GHIssue> ghIssues;
    private List<String> processedIssueNumbers;

    public TaskProcessor(String project){
        projectPath = project;
        projectName = project.split("/")[1];
    }

    public void run() throws IOException, GitAPIException {
        System.out.println("<Task Run Initiated>");

//        getPullRequestIds();

        getProcessedIssues();

        getIssues();

//        printResults();
    }

    private void getPullRequestIds() throws FileNotFoundException {
        PullRequestLister pullRequestLister = new PullRequestLister(projectName);
        if(!pullRequestLister.csvExists()){
            pullRequestLister.fetchPullRequestsFromResults();
        }
    }

    private void getProcessedIssues() {
        ProcessedIssueCollector processedIssueCollector = new ProcessedIssueCollector(projectName);
        processedIssueNumbers = processedIssueCollector.listProcessedIssues();
    }

    private void getIssues() throws IOException {
        Repo repo = RepoFetcher.fetchRepo(projectPath);

        PullRequestLister pullRequestLister = new PullRequestLister(projectName);
        ContentPrinter contentPrinter = new ContentPrinter(projectName);

        IssueCollector IssueCollector = new IssueCollector(contentPrinter, processedIssueNumbers, pullRequestLister);
        IssueCollector.fetchIssues(repo);
        ghIssues = IssueCollector.getIssues();
    }

//    private void printResults(){
//        System.out.println("<Printing results initiated>");
//    }
}
