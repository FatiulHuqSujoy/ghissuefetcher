package ghProcessors;

import utils.ContentPrinter;
import utils.FileProcessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class ProcessedIssueCollector {
    private String folderName;
    private final String DIR_NAME = "results/";

    public ProcessedIssueCollector(String projectName) {
        folderName = DIR_NAME + projectName;
    }

    public List<String> listProcessedIssues() {
        File folder = new File(folderName + "/issues");

        if(!folder.exists())
            return new ArrayList<>();

        List<String> processedIssues = FileProcessor.listFileNamesInFolder(folder, ".txt");
        return processedIssues;
    }
}
