package ghProcessors;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PullRequestLister {
    public static final String PR_CSV_DIR = "pull_requests/";
    String projectName;
    List<String> prIds;

    public PullRequestLister(String name) {
        projectName = name;
        prIds = new ArrayList<>();
    }

    public void fetchPullRequestsFromResults() throws FileNotFoundException {
        File folder =  new File("../GithubContentGraph/results/" + projectName + "/pull_requests");
        if(!folder.exists())
            return;

        List<String> prIds = new ArrayList<>();
        for(File file : folder.listFiles()){
            String name = file.getName().split("\\.")[0];
            prIds.add(name);
        }

        printPullRequestIds(prIds);
    }

    private void printPullRequestIds(List<String> prIds) throws FileNotFoundException {
        File file = new File(PR_CSV_DIR + projectName + ".csv");
        PrintWriter csvPRWriter = new PrintWriter(file);

        for(String name : prIds){
            csvPRWriter.println(name);
        }

        csvPRWriter.close();
    }

    public boolean csvExists() {
        return new File(PR_CSV_DIR + projectName + ".csv").exists();
    }

    public List<String> getPRIds() {
        return prIds;
    }

    public void populatePRIds() {
        File file = new File(PR_CSV_DIR + projectName + ".csv");

        String line = "";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            while ((line = br.readLine()) != null) {
                prIds.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isPR(String id){
        return prIds.contains(id);
    }

    public void addPRId(String id) throws FileNotFoundException {
        File file = new File(PR_CSV_DIR + projectName + ".csv");
        PrintWriter csvPRWriter = new PrintWriter(new FileOutputStream(file, true));

        csvPRWriter.append(id+"\n");

        csvPRWriter.close();
    }
}
