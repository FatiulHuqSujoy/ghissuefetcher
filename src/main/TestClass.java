package main;

import java.util.ArrayList;
import java.util.List;

public class TestClass {
    String str;
    int x;
    List<String> strList;
    List<TestClass2> testClass2List;

    public TestClass(String str1, int y){
        strList = new ArrayList<>();
        testClass2List = new ArrayList<>();
        str = str1;
        x = y;
    }

    public void addStr(String str){
        strList.add(str);
    }

    public void addTestClass2(TestClass2 testClass2){
        testClass2List.add(testClass2);
    }
}
