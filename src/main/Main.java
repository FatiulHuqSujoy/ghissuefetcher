package main;

import ghProcessors.ProjectLister;
import ghProcessors.TaskProcessor;
import org.eclipse.jgit.api.errors.GitAPIException;
import utils.JSONConverter;

import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> projects = ProjectLister.listProjects();

        for(String project : projects){
            TaskProcessor taskProcessor = new TaskProcessor(project);
            try {
                taskProcessor.run();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GitAPIException e) {
                e.printStackTrace();
            }
        }
    }

    private static void testJsonConverter() {
        TestClass testClass = new TestClass("WOAH", 2);
        for(int i=0; i<5; i++){
            testClass.addStr(i+"x");
            testClass.addTestClass2(new TestClass2(i+"x", i));
        }

        System.out.println(JSONConverter.jsonResult(testClass));
    }
}
